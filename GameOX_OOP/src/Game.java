import java.util.*;

public class Game {
	private Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player("O");
		x = new Player("X");
		board = new Board(x, o);

	}

	public void play() {
		showWelcome();
		board.printBoard();
		board.check();
		showStat();
		showGoodbye();
		playagain();
	}

	private void showWelcome() {
		System.out.println("Welcome to Game OX");
	}

	private void showStat() {
		System.out.println("Stat: ");
		System.out.println("   Player   " + "Win  " + " Lose  " + " Draw  ");
		System.out.println("     X       " + x.getWin() + "      "
				+ x.getLose() + "     " + x.getDraw() + "  ");
		System.out.println("     O       " + o.getWin() + "      "
				+ o.getLose() + "     " + o.getDraw() + "  ");

	}

	private void showGoodbye() {
		System.out.println("GoodBye.");
	}

	private void playagain() {
		Scanner kb = new Scanner(System.in);
		System.out.println("Do you want to play again? <yes/no>:");
		String answer = kb.next();
		if (answer.equalsIgnoreCase("yes")) {
			board = new Board(x, o);
			play();
		}else{
			System.out.println("Thank you for playing.");
		}
	}
}
