public class Player {
	private String name;
	private int win, lose, draw;

	public Player(String name) {
		this.name = name;
		win = 0;
		lose = 0;
		draw = 0;
	}

	public String getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

	public void setWin() {
		win++;
	}

	public void setLose() {
		lose++;
	}

	public void setDraw() {
		draw++;
	}

}
